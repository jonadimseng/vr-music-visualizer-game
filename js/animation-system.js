const mountain_speed = 0.25
const between_speed = 0.5
const street_speed = 0.75

AFRAME.registerComponent('groundstripe', {
    schema: {
        speed: {default: street_speed},
        ismoving: {default: true},
        spawnnew: {default: true}
    },
    init: function () {
        this.play()
    },
    update: function () {
    },
    tick: function () {
        if (this.data.ismoving) {
            this.el.object3D.position.z += this.data.speed
        }
        if (this.el.object3D.position.z > -720 && this.data.spawnnew) {
            this.data.spawnnew = false
            this.spawnnext()
        }
        if (this.el.object3D.position.z > 800 && this.data.ismoving) {
            this.data.ismoving = false
            this.remove()
            this.el.object3D.position.z = -750
        }

    },
    remove: function () {
        this.el.sceneEl.components.pool__groundstripe.returnEntity(this.el)
        this.data.ismoving = true
        this.data.spawnnew = true
    },
    spawnnext: function () {
        let sceneEl = document.querySelector('a-scene')
        let el = sceneEl.components.pool__groundstripe.requestEntity()
        el.play()
    }
})

AFRAME.registerComponent('roadmarking', {
    schema: {
        speed: {default: street_speed},
        ismoving: {default: true},
        spawnnew: {default: true}
    },
    init: function () {
        this.play()
    },
    update: function () {
    },
    tick: function () {
        if (this.data.ismoving) {
            this.el.object3D.position.z += this.data.speed
        }
        if (this.el.object3D.position.z > -720 && this.data.spawnnew) {
            this.data.spawnnew = false
            this.spawnnext()
        }
        if (this.el.object3D.position.z > 800 && this.data.ismoving) {
            this.data.ismoving = false
            this.remove()
            this.el.object3D.position.z = -750
        }

    },
    remove: function () {
        this.el.sceneEl.components.pool__roadmarking.returnEntity(this.el)
        this.data.ismoving = true
        this.data.spawnnew = true
    },
    spawnnext: function () {
        let sceneEl = document.querySelector('a-scene')
        let el = sceneEl.components.pool__roadmarking.requestEntity()
        el.play()
    }
})

AFRAME.registerComponent('mountain-l', {
    schema: {
        speed: {default: mountain_speed},
        ismoving: {default: true},
        spawnnew: {default: true}
    },
    init: function () {
        let num = Math.floor(Math.random() * 8 + 1);
        this.el.setAttribute('gltf-model', 'res/obj/Mountains/Mountain_' + num + '.gltf');
        this.play()
    },
    update: function () {
    },
    tick: function () {
        if (this.data.ismoving) {
            this.el.object3D.position.z += this.data.speed
        }
        if (this.el.object3D.position.z > -330 && this.data.spawnnew) {
            this.data.spawnnew = false
            this.spawnnext()
        }
        if (this.el.object3D.position.z > 800 && this.data.ismoving) {
            this.data.ismoving = false
            this.remove()
            this.el.object3D.position.z = -750
        }

    },
    remove: function () {
        this.el.sceneEl.components.pool__mountainsl.returnEntity(this.el)
        this.data.ismoving = true
        this.data.spawnnew = true
    },
    spawnnext: function () {
        let sceneEl = document.querySelector('a-scene')
        let el = sceneEl.components.pool__mountainsl.requestEntity()
        el.play()
    }
})

AFRAME.registerComponent('mountain-r', {
    schema: {
        speed: {default: mountain_speed},
        ismoving: {default: true},
        spawnnew: {default: true}
    },
    init: function () {
        var num = Math.floor(Math.random() * 8 + 1);
        this.el.setAttribute('gltf-model', 'res/obj/Mountains/Mountain_' + num + '.gltf');
        this.play()
    },
    update: function () {
    },
    tick: function () {
        if (this.data.ismoving) {
            this.el.object3D.position.z += this.data.speed
        }
        if (this.el.object3D.position.z > -330 && this.data.spawnnew) {
            this.data.spawnnew = false
            this.spawnnext()
        }
        if (this.el.object3D.position.z > 800 && this.data.ismoving) {
            this.data.ismoving = false
            this.remove()
            this.el.object3D.position.z = -750
        }
    },
    remove: function () {
        this.el.sceneEl.components.pool__mountainsr.returnEntity(this.el)
        this.data.ismoving = true
        this.data.spawnnew = true
    },
    spawnnext: function () {
        let sceneEl = document.querySelector('a-scene')
        let el = sceneEl.components.pool__mountainsr.requestEntity()
        el.play()
    }
})

AFRAME.registerComponent('randomcars', {
    schema: {
        speed: {default: street_speed},
        ismoving: {default: true},
        spawnnew: {default: true},
        removeat: {default: 800}
    },
    init: function () {
        this.randomizer()
        this.play()
    },
    update: function () {
    },
    tick: function () {
        if (this.data.ismoving) {
            this.el.object3D.position.z += this.data.speed
        }
        if (this.el.object3D.position.z > this.data.removeat) {
            this.data.ismoving = false
            this.data.spawnnew = false
            this.spawnnext()
            this.remove()
        }
    },
    remove: function () {
        this.el.sceneEl.components.pool__cars.returnEntity(this.el)
        this.data.ismoving = true
        this.data.spawnnew = true
        this.randomizer()
    },
    spawnnext: function () {
        let sceneEl = document.querySelector('a-scene')
        let el = sceneEl.components.pool__cars.requestEntity()
        el.play()
    },
    randomizer: function () {
        this.el.setAttribute('randomcars', 'removeat', Math.floor(Math.random() * (3200 - 800) + 800))

        let models = ["car_broken_purple_fixed", "car_broken_red_fixed", "car_healthy_purple_fixed", "car_healthy_red_fixed"];
        let random = Math.floor(Math.random() * models.length);
        this.el.setAttribute('gltf-model', 'res/obj/' + models[random] + '.gltf');

        let position = Math.random();
        if (position < 0.5) {
            this.el.setAttribute('position', {x: 25, y: 2.2, z: -750});
        } else {
            this.el.setAttribute('position', {x: -25, y: 2.2, z: -750});
        }

        this.el.object3D.rotation.y = Math.random() * (360 - 0) + 0;
    }
})

AFRAME.registerComponent('randomtrees', {
    schema: {
        speed: {default: between_speed},
        ismoving: {default: true},
        spawnnew: {default: true},
        removeat: {default: 800}
    },
    init: function () {
        this.randomizer()
        this.play()
    },
    update: function () {
    },
    tick: function () {
        if (this.data.ismoving) {
            this.el.object3D.position.z += this.data.speed
        }

        if (this.el.object3D.position.z > -500 && this.data.spawnnew) {
            this.data.spawnnew = false
            this.spawnnext()
        }
        if (this.el.object3D.position.z > this.data.removeat && this.data.ismoving) {
            this.data.ismoving = false
            this.remove()
        }
    },
    remove: function () {
        this.el.sceneEl.components.pool__trees.returnEntity(this.el)
        this.data.ismoving = true
        this.data.spawnnew = true
        this.randomizer()
    },
    spawnnext: function () {
        let sceneEl = document.querySelector('a-scene')
        let el = sceneEl.components.pool__trees.requestEntity()
        el.play()
    },
    randomizer: function () {
        this.el.setAttribute('randomtrees', 'removeat', Math.floor(Math.random() * (1000 - 800) + 800))

        let position = Math.random();
        if (position < 0.5) {
            this.el.setAttribute('position', {x: 64, y: -2, z: -750});
        } else {
            this.el.setAttribute('position', {x: -65, y: -2, z: -750});
        }

        this.el.object3D.rotation.y = Math.random() * (360 - 0) + 0;
    }
})

AFRAME.registerComponent('spawner-system', {
    init: function () {
        let components = Object.keys(this.el.sceneEl.components)
        let pools = components.filter(value => /^pool__/.test(value))
        pools.forEach(poolName => {
            let starterEntity = this.el.sceneEl.components[poolName].requestEntity()
            console.log(poolName)
            starterEntity.play()
        })
        console.log('SPAWNING SYSTEM ENGAGED')
    }
});

/*
AFRAME.registerComponent('musicbox', {
    schema: {
        speed: {default: street_speed},
        ismoving: {default: true},
        spawnnew: {default: true},
        removeat: {default: 800}
    },
    init: function () {
        this.play()
    },
    spawnnext: function () {
        let sceneEl = document.querySelector('a-scene')
        let el = sceneEl.components.pool__boxes.requestEntity()
        el.play()
    },
    remove: function () {
        this.el.sceneEl.components.pool__boxes.returnEntity(this.el)
        this.data.ismoving = true
        this.data.spawnnew = true
    },
    tick: function () {
        if (this.data.ismoving) {
            this.el.object3D.position.z += this.data.speed
        }

        if (this.el.object3D.position.z > -500 && this.data.spawnnew) {
            this.data.spawnnew = false
            this.spawnnext()
        }
        if (this.el.object3D.position.z > this.data.removeat && this.data.ismoving) {
            this.data.ismoving = false
            this.remove()
        }
    }
});*/
