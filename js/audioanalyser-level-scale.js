AFRAME.registerComponent('audioanalyser-level-scale', {
    schema: {
        analyserEl: {type: 'selector'},
        isFlat: {default: false},
        max: {default: 30},
        multiplier: {type: 'number', default: 1},
    },

    tick: function () {
        let analyserEl;
        let children;
        let data = this.data;
        let levels;

        analyserEl = data.analyserEl || this.el;
        levels = analyserEl.components.audioanalyser.levels;
        if (!levels) { return; }

        children = this.el.children;
        for (let i = 0; i < children.length; i++) {
            children[i].setAttribute('scale', {
                x: 1,
                y: Math.min(data.max, Math.max(levels[i] * data.multiplier, 0.05)),
                z: 1
            });
        }
    }

    /*tick: function () {
        let analyserEl = this.data.analyserEl || this.el;
        let el = this.el;
        let levels = analyserEl.components.audioanalyser.levels;
        if (!levels) {
            return;
        }

        let total = levels.reduce((sum, current) =>  sum + current, 0);
        let average = (total/levels.length);

        if (this.data.isFlat) {
            el.setAttribute('scale', {
                x: levels[this.data.f_number] * this.data.multiplier,
                y: levels[this.data.f_number] * this.data.multiplier
            })
        } else {
            el.setAttribute('scale', {
                x: average,
                y: average,
                z: average
            })
        }
    }*/
});
