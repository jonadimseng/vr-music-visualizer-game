AFRAME.registerComponent('foo', {
    init: function () {
        document.getElementById('ambience').play()
        let pieces = document.getElementsByClassName('domino');
        this.el.addEventListener('click', e => {
            sceneEl = document.querySelector('a-scene');
            sceneEl.emit("startMusic")
            for(let piece of pieces) {
                piece.emit('go')
                setTimeout(() => {
                    piece.emit('secondGo');
                }, 1000); //delay second animation
            }
        });
    }
})

//selecting a different song stops previous playing song
AFRAME.registerComponent('dieforyou', {
    update: function () {
        let element = this
        this.el.addEventListener('click', function (event) {
            document.getElementById('ambience').pause()
            element.playSong();
        });
    },
    playSong: function () {
        let lreactors = document.querySelectorAll('.level_react')
        lreactors.forEach(item => {
            item.setAttribute('audioanalyser-level-scale', {analyserEl: '#die'});
        });

        let vreactors = document.querySelectorAll('.volume_react')
        vreactors.forEach(item => {
            item.setAttribute('audioanalyser-volume-scale', {analyserEl: '#die'});
        });
        let cbeat = document.querySelectorAll('.color_beat')
        cbeat.forEach(item => {
            item.setAttribute('color-on-beat', {analyserEl: '#die'});
        });
        document.getElementById('diesong').play()
    }
});

AFRAME.registerComponent('mentalstate', {
    init: function () {
        let element = this
        this.el.addEventListener('click', function (event) {
            document.getElementById('ambience').pause()
            element.playSong();
        });
    },
    playSong: function () {
        let lreactors = document.querySelectorAll('.level_react')
        lreactors.forEach(item => {
            item.setAttribute('audioanalyser-level-scale', {analyserEl: '#mental'});
        });

        let vreactors = document.querySelectorAll('.volume_react')
        vreactors.forEach(item => {
            item.setAttribute('audioanalyser-volume-scale', {analyserEl: '#mental'});
        });
        let cbeat = document.querySelectorAll('.color_beat')
        cbeat.forEach(item => {
            item.setAttribute('color-on-beat', {analyserEl: '#mental'});
        });
        document.getElementById('mentalsong').play()
    }
});

AFRAME.registerComponent('livetowin', {
    update: function () {
        let element = this
        this.el.addEventListener('click', function (event) {
            document.getElementById('ambience').pause()
            element.playSong();
        });
    },
    playSong: function () {
        let lreactors = document.querySelectorAll('.level_react')
        lreactors.forEach(item => {
            item.setAttribute('audioanalyser-level-scale', {analyserEl: '#live'});
        });

        let vreactors = document.querySelectorAll('.volume_react')
        vreactors.forEach(item => {
            item.setAttribute('audioanalyser-volume-scale', {analyserEl: '#live'});
        });
        let cbeat = document.querySelectorAll('.color_beat')
        cbeat.forEach(item => {
            item.setAttribute('color-on-beat', {analyserEl: '#live'});
        });
        document.getElementById('livesong').play()
    }
});