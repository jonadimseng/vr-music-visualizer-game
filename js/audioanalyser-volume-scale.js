AFRAME.registerComponent('audioanalyser-volume-scale', {
    schema: {
        analyserEl: {type: 'selector'},
        isFlat: {default: false},
        max: {default: 20},
        multiplier: {type: 'number', default: 10},
    },

    tick: function () {
        let analyserEl = this.data.analyserEl || this.el;
        let el = this.el;

        let analyserComponent = analyserEl.components.audioanalyser;
        if (!analyserComponent.analyser) {
            return;
        }

        let volume = analyserComponent.volume * this.data.multiplier;
        if (this.data.isFlat) {
            el.setAttribute('scale', {
                x: volume,
                y: volume
            })
        } else {
            el.setAttribute('scale', {
                x: volume,
                y: volume,
                z: volume
            })
        }
        ;
    },
});
