AFRAME.registerComponent('color-on-beat', {
    schema: {
        analyserEl: {type: 'selector'},
        beat: {default: 'bass'},
    },

    init: function () {
        const analyserEl = this.data.analyserEl || this.el;
        const el = this.el;

        analyserEl.addEventListener(`audioanalyser-beat-${this.data.beat}`, function () {
          let newColor = new THREE.Color(Math.random(), Math.random(), Math.random()).getHexString()
            el.setAttribute(
                'material',
                'color',
                '#' + newColor
            );
          el.setAttribute(
              'material',
              'emissive',
              '#' + newColor
          );
            el.setAttribute(
                'light',
                'color',
                '#' + newColor
            );
        });
    },
});
